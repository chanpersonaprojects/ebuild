# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-libs/libpng/libpng-1.5.10.ebuild,v 1.9 2012/04/28 18:59:10 armin76 Exp $

EAPI=4

inherit eutils libtool multilib

DESCRIPTION="Portable Network Graphics library"
HOMEPAGE="http://www.libpng.org/"
SRC_URI="http://freefr.dl.sourceforge.net/project/libpng/libpng14/1.4.12/libpng-1.4.12.tar.gz"

LICENSE="as-is"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 m68k ~mips ppc ppc64 s390 sh sparc x86 ~amd64-fbsd ~sparc-fbsd ~x86-fbsd ~x64-freebsd ~x86-freebsd ~x86-interix ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris ~x86-winnt"
IUSE="neon static-libs"

RDEPEND="sys-libs/zlib"
DEPEND="${RDEPEND}
	app-arch/xz-utils"

DOCS=( ANNOUNCE CHANGES libpng-manual.txt README TODO )

pkg_setup() { 
   echo "WORKDIR$WORKDIR"
    echo "P=$P"
    P=libpng-1.4.12
}
    
src_unpack(){
    unpack ${A}
    mv /var/tmp/portage/media-libs/libpng14-1.4.12/work/libpng-1.4.12 /var/tmp/portage/media-libs/libpng14-1.4.12/work/libpng14-1.4.12
}

src_prepare() {
	elibtoolize
}

src_configure() {
	econf \
		$(use_enable static-libs static) \
		$(use_enable neon arm-neon)
}

src_install() {
    emake DESTDIR="${D}" install || die "Install failed"
}

#pkg_preinst() {
#	has_version ${CATEGORY}/${PN}:1.4 && return 0
#	preserve_old_lib /usr/$(get_libdir)/libpng14$(get_libname 14)
#}

#pkg_postinst() {
#	has_version ${CATEGORY}/${PN}:1.4 && return 0
#	preserve_old_lib_notify /usr/$(get_libdir)/libpng14$(get_libname 14)
#}
